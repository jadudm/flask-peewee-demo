######################################################
# IMPORTS
######################################################
# Python 2/3 compat
from __future__ import print_function
import os
# We need a bunch of Flask stuff
from flask import Flask
from flask import render_template
from flask import redirect
from flask import request
from flask import url_for
from flask import g
# We need to import the DB object
from models import db

# FIXME: Don't forget to import your own models!
# from models import ...

######################################################
# SETUP
######################################################
# Set up the Flask app
app = Flask(__name__)

# We have to set up and break down the DB on every request.
@app.before_request
def before_request():
  g.db = db
  g.db.connect()

@app.after_request
def after_request(response):
  g.db.close()
  return response

# Run this from the command line once.  
def create_tables():
    db.connect()
    # FIXME: Insert models to be created in the DB here.
    db.create_tables([])


######################################################
# ROUTES
######################################################
# FIXME: Add some routes

# start the server with the 'run()' method
if __name__ == '__main__':
  app.run(host = os.getenv('IP'), port = int(os.getenv('PORT')), debug = True)
