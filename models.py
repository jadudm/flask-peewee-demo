from peewee import *

# Create a database
# FIXME: Provide a filename for the database
db = SqliteDatabase('data/...', threadlocals = True)

class BaseModel (Model):
  class Meta:
    database = db 

######################################################
# MODELS
######################################################
# FIXME: Create some models